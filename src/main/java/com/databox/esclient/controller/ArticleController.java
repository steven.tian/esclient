package com.databox.esclient.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.databox.esclient.model.Article;
import com.databox.esclient.service.ElasticService;

@RestController
@RequestMapping("/v1/article")
public class ArticleController {
	
	private final ElasticService elasticService;

    public ArticleController(ElasticService elasticService) {
        this.elasticService = elasticService;
    }
	
    @GetMapping(value = "/query")
    public List<String> getContentsByKeyword(@RequestParam(value = "keyword") String keyword) {
        return elasticService.getContents(keyword);
    }
    
    @PostMapping(value = "/save")
    public Article save(@RequestBody Article article) {
        return elasticService.save(article);
    }
    
}
