package com.databox.esclient.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.databox.esclient.service.DictionaryService;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/v1/dictionary")
public class DictionaryController {

	DictionaryService dictService;

	public DictionaryController(DictionaryService dictService) {
		this.dictService = dictService;
	}

	@GetMapping(value = "/findAll")
	public List<String> findAll() {
		return dictService.findAll();
	}

	@PostMapping(value = "/save")
	public String save(@RequestParam(value = "term") String term) {
		dictService.save(term);
		return term;
	}

	@DeleteMapping(value = "/deleteTerm")
	public String deleteTerm(@RequestParam(value = "term") String term) {
		dictService.delete(term);
		return term;
	}

	@GetMapping(value = "/reload")
	public String reload(HttpServletResponse response) {
		String ans = String.join("\n", findAll());
		response.addHeader("Last-Modified", String.valueOf(System.currentTimeMillis()));
		return ans;
	}

}
