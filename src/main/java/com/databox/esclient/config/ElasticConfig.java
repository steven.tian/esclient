package com.databox.esclient.config;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;

import co.elastic.clients.transport.TransportUtils;

@Configuration
public class ElasticConfig extends ElasticsearchConfiguration {

	@Value("${elasticsearch.host}")
	private String host;

	@Value("${elasticsearch.port}")
	private String port;

	@Value("${elasticsearch.username}")
	private String name;

	@Value("${elasticsearch.pwd}")
	private String pwd;

	@Value("${elasticsearch.ca.fingerprint}")
	private String fingerPrint;

	@Override
	public ClientConfiguration clientConfiguration() {

		SSLContext sslContext = TransportUtils.sslContextFromCaFingerprint(fingerPrint);
		
		return ClientConfiguration.builder().connectedTo(host + ":" + port).usingSsl(sslContext , NoopHostnameVerifier.INSTANCE)
				.withBasicAuth(name, pwd).build();

	}
	
}
