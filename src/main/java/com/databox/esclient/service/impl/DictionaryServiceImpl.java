package com.databox.esclient.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.databox.esclient.model.Dictionary;
import com.databox.esclient.mssql.repository.DictionaryRepository;
import com.databox.esclient.service.DictionaryService;

import jakarta.transaction.Transactional;

@Service
public class DictionaryServiceImpl implements DictionaryService {

	@Autowired
	DictionaryRepository dictRepo;

	@Override
	public List<String> findAll() {
		List<String> ans = new ArrayList<>();
		dictRepo.findAll().forEach(d -> {
			ans.add(d.getTerm());
		});
		return ans;

	}

	@Override
	public void save(String term) {	
		dictRepo.save(new Dictionary(term));
	}

	@Override
	@Transactional
	public void delete(String term) {
		dictRepo.deleteByTerm(term);
	}
	
}
