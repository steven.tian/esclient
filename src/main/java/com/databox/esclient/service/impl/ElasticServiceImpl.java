package com.databox.esclient.service.impl;

import static co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders.match;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.DocumentOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import com.databox.esclient.model.Article;
import com.databox.esclient.service.ElasticService;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;

@Service
public class ElasticServiceImpl implements ElasticService {
	
	@Autowired
	ElasticsearchOperations operations; 
	
	@Autowired
	DocumentOperations docOperations;

	@Override
	public List<String> getContents(String keyword) {
		List<String> result = new ArrayList<>();
		Query criteria = QueryBuilders
				.bool(builder -> builder.must(match(article -> article.field(Article.FIELD_TEXT).query(keyword))));

		operations.search(NativeQuery.builder().withQuery(criteria).build(), Article.class)
				.forEach(searchHit -> {
					result.add(searchHit.getContent().getText());
				});

		return result;
	}

	@Override
	public Article save(Article entity) {
		return docOperations.save(entity);
	}
}
