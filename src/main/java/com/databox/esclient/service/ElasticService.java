package com.databox.esclient.service;

import java.util.List;

import com.databox.esclient.model.Article;


public interface ElasticService {

	List<String> getContents(String keyword);
	
	Article save(Article entity);
}
