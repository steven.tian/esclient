package com.databox.esclient.service;

import java.util.List;


public interface DictionaryService {

	List<String> findAll();
	
	void save(String term);
	
	void delete(String term);
	
}
