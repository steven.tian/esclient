package com.databox.esclient.mssql.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.databox.esclient.model.Dictionary;

@Repository
public interface DictionaryRepository extends CrudRepository<Dictionary, Integer>{
	int deleteByTerm(String term);
}
